;;; __PROJECT-NAME__.el ---  __PROJECT-NAME__            -*- lexical-binding: t; -*-

;; Copyright (C) __(format-time-string "%Y")__ Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: __(format-time-string "%d %b %Y")__
;; Version: 0.0.1
;; Keywords: 
;; URL: 
;; Package-Requires: ((emacs "26.3"))
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires


(provide '__PROJECT-NAME__)
;;; __PROJECT-NAME__.el ends here
