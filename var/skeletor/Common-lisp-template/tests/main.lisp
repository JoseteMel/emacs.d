(defpackage __PROJECT-NAME__/tests/main
  (:use :cl
        :__PROJECT-NAME__
        :rove))
(in-package :__PROJECT-NAME__/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :__PROJECT-NAME__)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
